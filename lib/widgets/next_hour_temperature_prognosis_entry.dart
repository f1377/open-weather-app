import 'package:flutter/material.dart';

class NextHourTemperaturePrognosisEntry extends StatelessWidget {
  final String time;
  final IconData icon;
  final String temp;

  const NextHourTemperaturePrognosisEntry({
    Key? key,
    required this.time,
    required this.temp,
    this.icon = Icons.ac_unit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 50.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            time,
            style: const TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),
          ),
          Icon(
            icon,
            size: 10.0,
          ),
          Text(
            temp,
            style: const TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}
