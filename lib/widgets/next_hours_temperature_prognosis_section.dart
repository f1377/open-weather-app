import 'package:flutter/material.dart';
import 'package:open_weather_app/widgets/next_hour_temperature_prognosis_entry.dart';

class NextHoursTemperaturePrognosisSection extends StatelessWidget {
  NextHoursTemperaturePrognosisSection({Key? key}) : super(key: key);

  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      controller: _scrollController,
      interactive: true,
      child: ListView(
        controller: _scrollController,
        scrollDirection: Axis.horizontal,
        children: const [
          NextHourTemperaturePrognosisEntry(time: '04:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '05:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '06:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '07:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '08:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '09:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '10:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '11:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '12:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '13:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '14:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '15:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '16:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '17:00', temp: '9 °C'),
          NextHourTemperaturePrognosisEntry(time: '18:00', temp: '9 °C'),
        ],
      ),
    );
  }
}
