import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final AsyncSnapshot snapshot;

  const ErrorScreen({
    Key? key,
    required this.snapshot,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Data not found!'),
      ),
      body: Center(child: Text('${snapshot.error}')),
    );
  }
}
