import 'package:flutter/material.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: Column(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 8),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: Colors.grey.shade200),
              ),
            ),
            child: ListTile(
              title: const Text('Difference weather?'),
              trailing: const Icon(
                Icons.arrow_forward_ios_sharp,
                size: 15.0,
              ),
              onTap: () {
                // open weather settings
              },
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 8),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: Colors.grey.shade200),
              ),
            ),
            child: ListTile(
              title: const Text('Customize units'),
              trailing: const Icon(
                Icons.arrow_forward_ios_sharp,
                size: 15.0,
              ),
              onTap: () {
                // open units settings
              },
            ),
          ),
          const ListTile(
            title: Text('Data'),
            trailing: Text(
              'One Call API',
              style: TextStyle(fontSize: 16.0),
            ),
          ),
          const ListTile(
            title: Text(
                'All the data for OpenWeather App is provided by One Call Api. OpenWeather aggregates and processes meteorological data from tens of thousands of weather stations, on-ground radars and satellites to bring you accurate and actionable weather data for any location worldwide.'),
          ),
          const ListTile(
            title: Align(
              alignment: Alignment.centerLeft,
              child: Icon(Icons.cake),
            ),
          )
        ],
      ),
    );
  }
}
