import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_weather_app/config.dart';
import 'package:open_weather_app/models/weather.dart';

class WeatherService {
  Future<Weather> fetchWeather() async {
    Uri uri = Uri.parse(kApiUrl);
    var response = await http.get(uri);

    if (response.statusCode == 200) {
      var weatherData = Weather.fromJson(jsonDecode(response.body));
      return weatherData;
    } else {
      throw Exception('Failed to load weather');
    }
  }
}
